import images1 from "../images/ThePinnacle.jpg";
import images2 from "../images/ClimbingSchool.jpg";
import images3 from "../images/cashFlow.jpg";
import images4 from "../images/MassageSecrets.jpg";
import images5 from "../images/Humanality.jpg";
import images6 from "../images/CryingRock.jpg";
import images7 from "../images/BigGuy.jpg";

export default [images1, images2, images3, images4, images5, images6, images7];